const express = require('express');

const productController = require('../controllers/product-controller');
const auth = require('../middlewares/auth');
const { uploader } = require('../middlewares/imgAdress');

const router = express.Router();



//auth.grantAccess('readAny', 'agrupadorData') metodo get

router.get('/getOneProduct/:productId', productController.getOneProduct);
router.get('/getAllProducts', productController.getAllProducts);
router.put('/updateOneProduct/:productId', /*auth.allowIfLoggedin, auth.grantAccess('updateOwn', 'adminData') */productController.updateOneProduct);
router.post('/createOneProduct/', /*auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData') */ productController.createOneProduct);
router.post('/createOneProductWithImg/', /*auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'),*/ uploader.single('file'), productController.createOneProductWithImg);
router.post('/createManyProduct/', /*auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData') ,*/ productController.createManyProduct);
router.delete('/deleteOneOnlyProduct/:productId', /*auth.allowIfLoggedin, auth.grantAccess('deleteOwn', 'adminData'),*/ productController.deleteOneOnlyProduct);



module.exports = router;