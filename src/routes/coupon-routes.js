const express = require('express');

const couponController = require('../controllers/coupon-controller');
const auth = require('../middlewares/auth');
const router = express.Router();



//auth.grantAccess('readAny', 'agrupadorData') metodo get

router.get('/getOneCoupon/:couponId', couponController.getOneCoupon);
router.get('/getAllCoupon', couponController.getAllCoupon);
router.put('/updateOneCoupon/:couponId', couponController.updateOneCoupon);
router.post('/createOneCoupon/', couponController.createOneCoupon);
router.post('/createManyCoupon/', couponController.createManyCoupon);
router.delete('/deleteOneCoupon/:couponId', couponController.deleteOneCoupon);



module.exports = router;