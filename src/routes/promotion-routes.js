const express = require('express');

const promotionController = require('../controllers/promotion-controller');
const auth = require('../middlewares/auth');
const { uploader } = require('../middlewares/imgAdress');

const router = express.Router();


router.get('/getOnePromotion/:promotionId', promotionController.getOnePromotion);

//probada
router.get('/getActivePromotion', promotionController.getActivePromotions);
router.get('/getAllPromotion', promotionController.getAllPromotions);

router.put('/updateOnePromotion/:promotionId', auth.allowIfLoggedin, auth.grantAccess('updateOwn', 'adminData'), promotionController.updateOnePromotion);

//probada
router.post('/createOnePromotion/', auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'), promotionController.createOnePromotion);

//Probada 
router.post('/createOnePromotionWithImg/', auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'), uploader.single('file'), promotionController.createOnePromotionWithImg);
//Probada
router.post('/createManyPromotion/', auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'), promotionController.createManyPromotion);
//Probada
router.delete('/deleteOneOnlyPromotion/:promotionId', auth.allowIfLoggedin, auth.grantAccess('deleteOwn', 'adminData'), promotionController.deleteOneOnlyPromotion);



module.exports = router;