const productRoutes = require('./product-routes');
const promotionRoutes = require('./promotion-routes');
const shoppingCartRoutes = require('./shoppingCart-routes');
const couponRoutes = require('./coupon-routes');
const userRoutes = require('./user-routes');

// Cosas de auth
const jwt = require('jsonwebtoken');
const User = require('../mongo/models/user');
const express = require('express');


module.exports = app => {
    app.use(async(req, res, next) => {
        try {
            if (req.headers["x-access-token"]) {
                const accessToken = req.headers["x-access-token"];
                const { userId, exp } = await jwt.verify(accessToken, process.env.JWT_SECRET);
                // Check if token has expired
                if (exp < Date.now().valueOf() / 1000) {
                    return res.status(401).json({ error: "JWT token has expired, please login to obtain a new one" });
                }
                res.locals.loggedInUser = await User.findById(userId);
                next();
            } else {
                next()
            }
        } catch (e) {
            res.status(403).send({ status: 'error', message: e.message });
            return
        }
    });



    const rootApiPath = '/atlas/api';
    const apiVersion = 'v1';
    console.log("endpoints will be at " + rootApiPath + '/' + apiVersion)
    console.log("algo rapido");

    app.use(rootApiPath + '/' + apiVersion + '/' + 'product', productRoutes);
    app.use(rootApiPath + '/' + apiVersion + '/' + 'promotion', promotionRoutes);
    app.use(rootApiPath + '/' + apiVersion + '/' + 'shoppingCart', shoppingCartRoutes);
    app.use(rootApiPath + '/' + apiVersion + '/' + 'coupon', couponRoutes);
    app.use(rootApiPath + '/' + apiVersion + '/' + 'user', userRoutes);


}