const express = require('express');

const userController = require('../controllers/user-controller');
const auth = require('../middlewares/auth')

const router = express.Router();


// SOLO PARA DESARROLLO!, A producción hay que sacar comentarios //
router.get('/get', /* auth.allowIfLoggedin, auth.grantAccess("updateAny", "usuarioData"), */ userController.getUsuarios);
router.get('/get/:usuarioId', /*auth.allowIfLoggedin, auth.grantAccess("updateAny", "usuarioData"), */ userController.findOneUsuario);
router.get('/verifyToken', userController.verifyToken);
router.put('/updateUsuarioContrasena/:usuarioId', auth.allowIfLoggedin, userController.updateUsuarioContrasena);
router.put('/updateUsuario/:usuarioId', /* auth.allowIfLoggedin, auth.grantAccess("updateAny", "usuarioData"),*/ userController.updateUsuario)
router.post('/sendCorreo', userController.sendCorreo);
router.post('/recuperarContrasena', userController.recuperarContrasena);
router.post('/signup', userController.signup);
router.post('/login', userController.login);


module.exports = router;