const express = require('express');

const shoppingCartController = require('../controllers/shoppingCart-controller');
const auth = require('../middlewares/auth');
const { uploader } = require('../middlewares/imgAdress');

const router = express.Router();



//auth.grantAccess('readAny', 'agrupadorData') metodo get

router.get('/getOneShoppingCart/:shoppingCartId', shoppingCartController.getOneShoppingCart);
router.get('/getAllShoppingCart', shoppingCartController.getAllShoppingCart);
router.put('/updateOneShoppingCart/:shoppingCartId', auth.allowIfLoggedin, auth.grantAccess('updateOwn', 'adminData'), shoppingCartController.updateOneShoppingCart);
router.post('/createOneShoppingCart/', auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'), shoppingCartController.createOneShoppingCart);
router.post('/createOneShoppingCartWithImg/', /*auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'),*/ uploader.single('file'), shoppingCartController.createOneShoppingCartWithImg);
router.post('/createManyShoppingCart/', auth.allowIfLoggedin, auth.grantAccess('createOwn', 'adminData'), shoppingCartController.createManyShoppingCart);
router.delete('/deleteOneShoppingCart/:shoppingCartId', auth.allowIfLoggedin, auth.grantAccess('deleteOwn', 'adminData'), shoppingCartController.deleteOneShoppingCart);



module.exports = router;