const AccessControl = require("accesscontrol");
const ac = new AccessControl();

/*
En cuanto a roles, pienso que 4 son el minimo:

persona: lee todos los datos de su propia persona (persona es la entidad en la db y cada una es un persona)
empresa: lee datos publicos de todas las personas que pertenecen a la empresa
agrupador: lee datos de todas las empresas que representa.
admin: lee todo

*/

exports.roles = (function() {

    ac.grant("cliente")
        .readOwn("clienteData")
        .updateOwn("clienteData")
        .createOwn("clienteData")
        .deleteOwn("clienteData")

    ac.grant("empleado")
        .extend("cliente")
        .readOwn("empleadoData")
        .updateOwn("empleadoData")
        .deleteOwn("empleadoData")
        .createOwn("empleadoData")


    ac.grant("admin")
        .extend("cliente")
        .extend("empleado")
        // privilegios sobre usuarios
        .readOwn("adminData")
        .updateOwn("adminData")
        .deleteOwn("adminData")
        .createOwn("adminData")
        // privilegios sobre cliente
        .readAny("clienteData")
        .updateAny("clienteData")
        .deleteAny("clienteData")
        // privilegios sobre empleado
        .readAny("empleadoData")
        .updateAny("empleadoData")
        .deleteAny("empleadoData")

    return ac;


})();