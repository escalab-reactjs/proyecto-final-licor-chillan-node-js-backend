const multer = require('multer');
const path = require('path');


const storage = multer.diskStorage({
    destination: function (request, file, cb) { //Indica la carpeta de destino

        const { url_img } = request.body
        console.log("url images: " + url_img);
        console.log("file : " + JSON.stringify(file));
        //opcion 1
        // cb(null, path.join(__dirname, `../../../proyecto-final-licor-chillan-react-js-frontend/src/assets/${url_img}`));

        //opcion2
        cb(null, path.join(__dirname, `../../../proyecto-final-licor-chillan-react-js-frontend/public/assets/${url_img}`));


    },
    filename: function (request, file, cb) { //Indica el nombre del archivo

        cb(null, `${file.fieldname}-${Date.now()}.svg`);
    }
});
const uploader = multer({ storage }); //Se guardara la imagen en el servidor

module.exports = { uploader };