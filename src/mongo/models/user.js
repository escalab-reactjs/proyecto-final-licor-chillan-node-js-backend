const mongoose = require('mongoose');

var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const userSchema = new Schema({
    rut: {
        type: String,
        require: true,
        unique: true,
    },
    email: {
        type: String,
        require: true,
        unique: true,
    },
    nombre: {
        type: String,
        require: true,
    },
    apellido: {
        type: String,

    },
    direccion: {
        type: String,

        enum: ["departamento", "domicilio"],
        infoDomicilio: {
            villa: {
                type: String,
                require: false,
            },
            calle: {
                type: String,
                require: false,
            },
            numero: {
                type: String,
                require: false,
            },
        },

        infoDepartamento: {
            villa: {
                type: String,
                require: false,
            },
            calle: {
                type: String,
                require: false,
            },
            numeroEdificion: {
                type: String,
                require: false,
            },
            numeroDepartamento: {
                type: String,
                require: false,
            },
            nombre: {
                type: String,
                require: false,

            },
        },
    },

    telefono: {
        type: String,
        require: false,
    },
    fechaNacimiento: {
        type: Date,

    },

    password: {
        type: String,
        required: false
    },

    role: {
        type: String,
        default: 'persona',
        enum: ["cliente", "vecino", "empleado", "admin"]
    },

    accessToken: {
        type: String
    },

    // perteneceB: {
    //     type: Schema.Types.ObjectId,
    //     replies: this,
    // },



}, {
    timestamps: true,
});


userSchema.plugin(uniqueValidator);
userSchema.plugin(idValidator)

const model = mongoose.model('User', userSchema);

module.exports = model;