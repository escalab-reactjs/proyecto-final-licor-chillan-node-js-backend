const mongoose = require('mongoose');

var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const shoppingCartSchema = new Schema({
    pertainTo: {
        type: Schema.Types.ObjectId,
        ref: "Usuario",
    },
    state: {
        type: String,
        default: "pendiente",
        enum: ["vendido", "pendiente"]
    },

    products: [{
        name: {
            type: String,
            require: true,
        },
        amount: {
            type: String,
            require: true,
        },
        brand: {
            type: String,
            require: true,
        },
        url_img: {
            type: String,
            require: true,
        },
        total_price_products: {
            type: String,
            require: true,
        },
    }],



    promotions: [{

        name: {
            type: String,
            require: true,
        },
        amount: {
            type: String,
            require: true,
        },
        cost_promotions: {
            type: String,
            require: true,
        },

        price_promotion: {
            type: String,
            require: true,
        },

        product: [{
            name: {
                type: String,
                require: true,
            },
            amount: {
                type: String,
                require: true,
            },
            brand: {
                type: String,
                require: true,
            },
            url_img: {
                type: String,
                require: true,
            },
            total_price_products: {
                type: String,
                require: true,
            },
        }],

    }],

    cost_products: {
        type: String,
        require: true,
    },

    cost_promotions: {
        type: String,
        require: true,
    },

    price_final: {
        type: String,
        require: true,
    }


}, {
    timestamps: true,
});


shoppingCartSchema.plugin(uniqueValidator);
shoppingCartSchema.plugin(idValidator)

const model = mongoose.model('shoppingCart', shoppingCartSchema);

module.exports = model;