const mongoose = require('mongoose');

var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const productSchema = new Schema({
    name: {
        type: String,
        require: true,
    },
    stock: {
        type: String,
        require: true,
    },
    url_img: {
        type: String,
        require: true,
    },
    price_unitary: {
        type: String,
        require: true,
    },
    brand: {
        type: String,
        require: true,
    },

}, {
    timestamps: true,
});


productSchema.plugin(uniqueValidator);
productSchema.plugin(idValidator)

const model = mongoose.model('Product', productSchema);

module.exports = model;