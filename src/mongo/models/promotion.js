const mongoose = require('mongoose');

var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const promotionSchema = new Schema({
    name: {
        type: String,
        require: true,
    },

    url_img: {
        type: String,
        require: true,
    },

    from_date: {
        type: Date,
        require: true
    },

    to_date: {
        type: Date,
        require: true
    },

    active: {
        type: Boolean,
        require: true
    },

    // es la suma total de todos los producto
    cost_promotion: {
        type: String,
        require: true,
    },

    // es el precio a cual venderemos 
    // la diferencia entre cost_promotion-price_promotion = a cuanto ahorra el cliente
    price_promotion: {
        type: String,
        require: true,
    },

    product: [{
        name: {
            type: String,

        },
        amout: {
            type: String,

        },
        brand: {
            type: String,

        },
        url_img: {
            type: String,

        },
        total_price_products: {
            type: String,

        },
    }],
    require: false,


}, {
    timestamps: true,
});


promotionSchema.plugin(uniqueValidator);
promotionSchema.plugin(idValidator)

const model = mongoose.model('promotion', promotionSchema);

module.exports = model;