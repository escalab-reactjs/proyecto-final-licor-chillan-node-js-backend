const mongoose = require('mongoose');

var idValidator = require('mongoose-id-validator');
const uniqueValidator = require('mongoose-unique-validator');

const { Schema } = mongoose;

const couponSchema = new Schema({
    codigo: {
        type: String,
        require: true,
        unique: true
    },
    tipo: {
        type: String,
        enum: ["viaje", "descuento", "especial"],
        require: true,
    },
    caracter: {
        type: String,
        enum: ["unico", "general"],
        require: true,
    },

    estado: {
        type: String,
        enum: ["activo", "desactivo", "utilizado", "no utilizado"],
        require: true,
    },

    precioDescuento: {
        type: String,
        require: false,
    },

}, {
    timestamps: true,
});


couponSchema.plugin(uniqueValidator);
couponSchema.plugin(idValidator)

const model = mongoose.model('Coupon', couponSchema);

module.exports = model;