const jwt = require('jsonwebtoken');
const auth = require('../middlewares/auth');
const shoppingCartModel = require('../mongo/models/shoppingCart');




// -------- funciones



const getOneShoppingCart = async(request, response) => {
    try {
        const { shoppingCartId } = request.params;

        const shoppingCart = await shoppingCartModel.find({ _id: shoppingCartId });


        response.status(200).send({ status: 'ok', data: shoppingCart });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};

const getAllShoppingCart = async(request, response) => {
    try {
        const shoppingCarts = await shoppingCartModel.find();
        response.status(200).send({ status: 'ok', data: shoppingCarts });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};


const updateOneShoppingCart = (request, response) => {
    // Validate Request

    try {

        if (!request.params.shoppingCartId) {
            return response.status(400).send({
                message: "productId can not be empty"
            });
        }
        // Find empresa and update it with the request body
        console.log("request.params.productId" + request.params.shoppingCartId);

        const { perteneceA, products, promotion, cost_products, cost_promotions, price_final } = request.body

        shoppingCartModel.findByIdAndUpdate(request.params.shoppingCartId, {
            ...request.body

        }, function(error, shoppingCarts) {
            if (error) {

                response.status(500).send({ status: 'error', message: error });
            } else {
                response.send(shoppingCarts);
            }
        });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error });
    }
};


const createOneShoppingCart = async(request, response) => {
    try {

        // const { role, id, accessToken } = request.user
        const { perteneceA, products, promotion, cost_products, cost_promotions, price_final } = request.body;

        await shoppingCartModel.create({
            ...request.body

        }, function(error, shoppingCart) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'product was created', data: product });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createOneShoppingCartWithImg = async(request, response) => {
    try {

        // const { role, id, accessToken } = request.user
        const { perteneceA, products, promotion, cost_products, cost_promotions, price_final } = request.body;
        const { file, body } = request

        console.log("file.path", file.path);
        console.log("body", body);
        const url_img = file.path;
        await shoppingCartModel.create({
            ...request.body,
            url_img
        }, function(error, shoppingCart) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'product was created', data: shoppingCart });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createManyShoppingCart = async(request, response) => {
    try {
        const list_shoppingCart = request.body;
        // const { role } = request.user
        // var id = request.params.Id
        // var objectId = new ObjectId(request.params.Id)


        shoppingCartModel.insertMany(list_shoppingCart, function(error, responseShoppingCart) {
            if (error) {

                response.status(500).send({ status: 'error', message: error.message });
            } else {

                response.send({ status: 'product was created', message: responseShoppingCart });
                return
            }
        })


    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO-UNA DE LAS EMPRESAS NO HA SIDO CREADA', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }


}

const deleteOneShoppingCart = (request, response) => {

    shoppingCartModel.findByIdAndDelete(request.params.shoppingCartId)
        .then(name => {
            if (!name) {
                return response.status(404).send({
                    message: "product not found with id " + request.params.shoppingCartId
                });
            }
            response.send({ message: "product " + request.params.shoppingCartId + " deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return response.status(404).send({
                    status: "error",
                    message: "product not found with id " + request.params.shoppingCartId
                });
            }
            return response.status(500).send({
                status: "error",
                message: "Could not delete product with id " + request.params.shoppingCartId
            });
        });
};


module.exports = {
    getOneShoppingCart,
    getAllShoppingCart,
    updateOneShoppingCart,
    createOneShoppingCart,
    createOneShoppingCartWithImg,
    createManyShoppingCart,
    deleteOneShoppingCart,

};