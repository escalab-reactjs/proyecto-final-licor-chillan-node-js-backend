const auth = require('../middlewares/auth')
const jwt = require('jsonwebtoken');
const nodemailer = require("nodemailer");

const { v1: uuidv1 } = require('uuid');

var userModel = require('../mongo/models/user');




// nuevo signup 
const signup = async(req, res, next) => {
    try {

        const { role } = req.body;

        if (role === "cliente") {
            const {
                rut,
                email,
                password,
                nombre,
                apellido,
                direccion,
                telefono,
                fechaNacimiento
            } = req.body
            const hashedPassword = await auth.hashPassword(password);

            const newUser = new userModel({
                rut: rut,
                email: email,
                password: hashedPassword,
                nombre: nombre,
                apellido: apellido,
                direccion: direccion,
                telefono: telefono,
                fechaNacimiento: fechaNacimiento,
                role: "cliente",
            });
            const accessToken = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
                expiresIn: "1d"
            });
            newUser.accessToken = accessToken;

            newUser.save(
                function(error, usuario) {
                    if (error) {
                        res.status(500).send({ status: 'error', message: error.message });
                    } else {
                        res.status(200).send({ status: "ok", data: usuario })
                    }
                }
            );
        } else if (role === "empleado") {
            const {
                rut,
                email,
                password,
                nombre,
                apellido,
                direccion,
                telefono,
                fechaNacimiento
            } = req.body
            const hashedPassword = await auth.hashPassword(password);
            const newUser = new userModel({
                rut: rut,
                email: email,
                password: hashedPassword,
                nombre: nombre,
                apellido: apellido,
                direccion: direccion,
                telefono: telefono,
                fechaNacimiento: fechaNacimiento,
                role: "empleado"
            });
            const accessToken = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
                expiresIn: "1d"
            });
            newUser.accessToken = accessToken;
            newUser.save(
                function(error, usuario) {
                    if (error) {
                        res.status(500).send({ status: 'error', message: error.message });
                    } else {

                        res.status(200).send({ status: "ok", data: usuario })
                    }
                }
            );

        } else if (role === "vecino") {
            const {
                rut,
                email,
                password,
                nombre,
                apellido,
                direccion,
                telefono,
                fechaNacimiento
            } = req.body
            const hashedPassword = await auth.hashPassword(password);
            const newUser = new userModel({
                rut: rut,
                email: email,
                password: hashedPassword,
                nombre: nombre,
                apellido: apellido,
                direccion: direccion,
                telefono: telefono,
                fechaNacimiento: fechaNacimiento,
                role: "vecino"
            });
            const accessToken = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
                expiresIn: "1d"
            });
            newUser.accessToken = accessToken;
            newUser.save(
                function(error, usuario) {
                    if (error) {
                        res.status(500).send({ status: 'error', message: error.message });
                    } else {

                        res.status(200).send({ status: "ok", data: usuario })
                    }
                }
            );

        } else if (role === "admin") {
            const {
                rut,
                email,
                password,
                nombre,
                apellido,
                direccion,
                telefono,
                fechaNacimiento
            } = req.body
            const hashedPassword = await auth.hashPassword(password);

            const newUser = new userModel({
                rut: rut,
                email: email,
                password: hashedPassword,
                nombre: nombre,
                apellido: apellido,
                direccion: direccion,
                telefono: telefono,
                fechaNacimiento: fechaNacimiento,
                role: "admin"
            });
            console.log("newUser", newUser._id);
            console.log(" process.env.JWT_SECRET", process.env.JWT_SECRET);
            const accessToken = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
                expiresIn: "1d"
            });
            newUser.accessToken = accessToken;

            newUser.save(
                function(error, usuario) {
                    if (error) {
                        res.status(500).send({ status: 'error', message: error.message });
                    } else {
                        res.status(200).send({ status: "ok", data: usuario })
                    }
                }
            );
        }

    } catch (error) {
        next(error)
    }
}


// nuevo login 
const login = async(request, response) => {

    try {

        const { email, password } = request.body;



        const user = await userModel.findOne({ email: email });
        if (!user) return next(new Error('Email does not exist'));
        const validPassword = await auth.validatePassword(password, user.password);
        if (!validPassword) return next(new Error('Password is not correct'))
        const accessToken = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
            expiresIn: "1d"
        });

        await userModel.findByIdAndUpdate(user._id, { accessToken })
        console.log("que tal")
        response.status(200).json({
            data: { id: user._id, email: user.email, role: user.role },
            accessToken
        })

    } catch (error) {
        console.log("error " + error);
        response.status(500).send({ status: 'error', message: error.message });
    }
}

const updateUsuario = (req, res) => {
    // Validate Request
    console.log("entro acá");
    if (!req.params.usuarioId) {
        return res.status(400).send({
            message: "We need a valid userid to update"
        });
    }
    console.log("entro acá");
    // Find usuario and update it with the request body
    userModel.findByIdAndUpdate(req.params.usuarioId, {
            ...req.body
        }, { new: true })
        .then(usuarioId => {
            if (!usuarioId) {
                return res.status(404).send({
                    message: "Usuario not found with id " + req.params.usuarioId
                });
            }
            res.send(usuarioId);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "Usuario not found with id " + req.params.usuarioId
                });
            }
            return res.status(500).send({
                message: "Error updating usuario with id " + req.params.usuarioId
            });
        });
};

const updateUsuarioContrasena = async(req, res) => {
    // Validate Request    
    try {
        const idUser = req.params.usuarioId;

        if (!idUser) {
            return res.status(400).send({
                message: "We need a valid userid to update"
            });
        }

        const user = await userModel.findOne({ _id: idUser });
        const { currentPassword, newPassword } = req.body;

        const hashedPassword = await auth.hashPassword(newPassword);
        const validPassword = await auth.validatePassword(currentPassword, user.password);
        if (!validPassword) {
            res.status(200).send({ status: 'error', message: 'password invalido' });

        } else {
            await userModel.findByIdAndUpdate(idUser, {
                password: hashedPassword,
            });
            res.status(200).send({ status: 'ok', message: 'user password updated' });

        }

    } catch (error) {
        res.status(500).send({ status: 'error', message: error });
    }
};

const recuperarContrasena = async(request, response) => {
    const { email } = request.body;
    const newContrasena = uuidv1();

    const hashedPassword = await auth.hashPassword(newContrasena);
    // const validPassword = await auth.validatePassword(currentPassword, user.password);


    await userModel.findOneAndUpdate({
        email: email
    }, {
        password: hashedPassword,
    }, function(error, newUsuario) {
        if (error) {


            response.status(500).send({ status: 'error', message: error.message });
        } else {

            if (newUsuario === null) {
                response.status(500).send({ status: 'error', message: 'El correo no existe en nuestra base de datos' });
            } else {

                sendCorreo(newUsuario, newContrasena);


                response.status(200).send({ status: 'ok', data: 'Hemos enviado un mensaje a su correo' });

            }
        }
    });
}

const findOneUsuario = async(request, response) => {
    try {
        const usuarios = await userModel.findById(request.params.usuarioId);
        response.status(200).send({ status: 'ok', data: usuarios });
    } catch (error) {
        response.status(500).send({ status: 'error', message: error.message });
    }

};

const getUsuarios = async(request, response) => {
    try {
        const usuarios = await userModel.find();
        response.status(200).send({ status: 'ok', data: usuarios });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};

//funcion correo
const sendCorreo = async(usuario, clave) => {
    // let testAccount = await nodemailer.createTestAccount();


    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: process.env.userMail,
            pass: process.env.passwordMail,
        },
    });

    // let transporter = nodemailer.createTransport({
    //     host: 'smtp.gmail.com',
    //     port: 587,
    //     secure: false,
    //     requireTLS: true,
    //     auth: {
    //         user: testAccount.user,
    //         pass: testAccount.pass,
    //     },
    // });
    const correoTo = usuario.email;
    var mailOptions = {
        from: '"Licor chillan 👻"', // sender address
        // to: "gianfranco@perk.cl, ignacio@perk.cl, ssantelicesg@gmail.com,sebastian.cornejo@gmail.com ,sabici2013@gmail.com", // list of receivers
        to: correoTo,
        subject: 'Creacion de cuenta de Perk',
        text: "Hello world?", // plain text body
        html: "<b>ingresa con su correo y su nueva contraseña :</b><br>" + "correo:" + correoTo + "<br>clave:" + clave + "<br> <a href='https://www.youtube.com/watch?v=qH7WrSwoD6c&list=RDqH7WrSwoD6c&start_radio=1' class='btn btn-success'>presione aquí para ingresar a la página</a>", // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            console.log(error);

        } else {
            console.log("Email sent");

        }
    });


};

// Verifica que la token sea válida y devuelve rol/id
const verifyToken = async(request, response) => {
    const token = request.headers["x-access-token"];
    if (token) {
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) {
                return response.json({ message: "Invalid token" });
            } else {
                userModel.findById(decoded.userId).then(data => {
                    return response.status(200).send({ status: "ok", role: data.role, id: data.id });
                }).catch(err => {
                    return response.sendStatus(500).send({
                        message: err.message || "Some error occured"
                    });
                })
            }
        });
    } else {
        response.send({ message: "Token not found in request" });
    }
}

// revisar 
const nombre = async(req, res, next) => {
    console.log("let's search the name for id ", req.params.usuarioId)
    userModel.findById(req.params.usuarioId, function(err, usuario) {
        if (err) {
            console.log("something went wrong looking for id", req.params.usuarioId)
            res.status(404).send({ status: "err", message: err.message })
            return;
        } else {
            console.log("user is a typical case of ", usuario.role)
            if (usuario.role === "persona") {
                personaModel.findById(usuario.personaAsociada, function(err, persona) {
                    if (err) {
                        console.log("something went wrong when looking for the person associated with user", req.params.usuarioId)
                        res.status(404).send({ status: "err", message: err.message })
                        return;
                    } else {
                        console.log("we found ", persona.id, "associated to ", usuario.id)
                        res.status(200).send({ status: "ok", nombre: persona.nombrePersona })
                    }
                });
            } else if (usuario.role === "empresa") {
                empresaModel.findById(usuario.empresaAsociada, function(err, empresa) {
                    if (err) {
                        console.log("something went wrong when looking for the empresa associated with user", req.params.usuarioId)
                        res.status(404).send({ status: "err", message: err.message })
                        return;
                    } else {
                        console.log("we found ", persona.id, "associated to ", usuario.id)
                        res.status(200).send({ status: "ok", nombre: empresa.nombreEmpresa })
                    }
                });
            } else if (usuario.role === "agrupador") {
                agrupadorModel.findById(usuario.agrupadorAsociado, function(err, agrupador) {
                    if (err) {
                        console.log("something went wrong when looking for the agrupador associated with user", req.params.usuarioId)
                        res.status(404).send({ status: "err", message: err.message })
                        return;
                    } else {
                        console.log("we found ", persona.id, "associated to ", usuario.id)
                        res.status(200).send({ status: "ok", nombre: agrupador.nombreAgrupador })
                    }
                });
            } else if (usuario.role === "admin") {
                res.status(200).send({ status: "ok", nombre: "Lord Perk" })
            } else {
                res.status(200).send({ status: "ok", nombre: usuario.email })
            }
        }
    });

}

module.exports = {
    signup,
    login,
    nombre,
    updateUsuario,
    findOneUsuario,
    getUsuarios,
    updateUsuarioContrasena,
    sendCorreo,
    recuperarContrasena,
    verifyToken
};