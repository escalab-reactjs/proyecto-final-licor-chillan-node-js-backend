const jwt = require('jsonwebtoken');
const auth = require('../middlewares/auth');
const promotionModel = require('../mongo/models/promotion');




// -------- funciones


const getOnePromotion = async (request, response) => {
    try {
        const { promotionId } = request.params;

        const promotion = await PromotionModel.find({ _id: promotionId });


        response.status(200).send({ status: 'ok', data: promotion });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};

const getAllPromotions = async (request, response) => {
    try {
        const promotions = await promotionModel.find();
        response.status(200).send({ status: 'ok', data: promotions });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};

const getActivePromotions = async (request, response) => {
    try {

        const promotions = await promotionModel.find({ active: true });


        response.status(200).send({ status: 'ok', data: promotions });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};



const updateOnePromotion = (request, response) => {
    // Validate Request

    try {

        if (!request.params.promotionId) {
            return response.status(400).send({
                message: "PromotionId can not be empty"
            });
        }
        // Find empresa and update it with the request body
        console.log("request.params.PromotionId" + request.params.promotionId);

        //const { stock, name } = request.body

        promotionModel.findByIdAndUpdate(request.params.promotionId, {
            ...request.body

        }, function (error, promotion) {
            if (error) {

                response.status(500).send({ status: 'error', message: error });
            } else {


                response.send(promotion);


            }
        });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error });
    }
};


const createOnePromotion = async (request, response) => {
    try {


        console.log("entro" + JSON.stringify(request.body));
        console.log("entro2" + request.body);

        const { name, cost_promotion, price_promotion, product, from_date, to_date } = request.body;

        console.log("name" + name);
        await promotionModel.create({
            ...request.body
        }, function (error, promotion) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'Promotion was created', data: promotion });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createOnePromotionWithImg = async (request, response) => {
    try {

        // const { role, id, accessToken } = request.user

        const { name, cost_promotion, price_promotion, product, from_date, to_date } = request.body;
        const { file, body } = request
        console.log("path", file);
        const url_img = file.path;


        await promotionModel.create({
            ...request.body,
            url_img
        }, function (error, promotion) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'Promotion was created', data: promotion });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createManyPromotion = async (request, response) => {
    try {
        const list_promotions = request.body;
        // const { role } = request.user
        // var id = request.params.Id
        // var objectId = new ObjectId(request.params.Id)


        promotionModel.insertMany(list_promotions, function (error, responsePromotions) {
            if (error) {
                response.status(500).send({ status: 'error', message: error.message });
            } else {
                response.send({ status: 'Promotion was created', message: responsePromotions });
                return
            }
        })


    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO-UNA DE LAS EMPRESAS NO HA SIDO CREADA', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }


}

const deleteOneOnlyPromotion = (req, res) => {
    console.log("entro a delete");

    promotionModel.findByIdAndDelete(req.params.promotionId)
        .then(name => {
            if (!name) {
                return res.status(404).send({
                    message: "Promotion not found with id " + req.params.promotionId
                });
            }
            res.send({ message: "Promotion " + req.params.promotionId + " deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "Promotion not found with id " + req.params.promotionId
                });
            }
            return res.status(500).send({
                message: "Could not delete Promotion with id " + req.params.promotionId
            });
        });
};


module.exports = {
    getOnePromotion,
    getActivePromotions,
    getAllPromotions,
    updateOnePromotion,
    createOnePromotion,
    createOnePromotionWithImg,
    createManyPromotion,
    deleteOneOnlyPromotion,

};