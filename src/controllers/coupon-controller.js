const jwt = require('jsonwebtoken');
const auth = require('../middlewares/auth');
const couponModel = require('../mongo/models/coupon');

const getOneCoupon = async (request, response) => {
    console.log('hola entro');
    try {
        const { cuponId } = request.params;

        const coupon = await couponModel.find({ _id: cuponId });

        response.status(200).send({ status: 'ok', data: coupon });
    } catch (error) {
        response.status(500).send({ status: 'error', message: error.message });
    }
};

const getAllCoupon = async (request, response) => {
    try {
        const coupon = await couponModel.find();
        response.status(200).send({ status: 'ok', data: coupon });
    } catch (error) {
        response.status(500).send({ status: 'error', message: error.message });
    }
};

const updateOneCoupon = (req, res) => {
    // Validate Request

    try {
        if (!req.params.couponId) {
            return res.status(400).send({
                status: 'error',
                message: 'cuponId can not be empty',
            });
        }
        // Find empresa and update it with the request body
        var objectCuponId = new ObjectId(req.params.cuponId);

        couponModel.findByIdAndUpdate(
            req.params.cuponId,
            {
                ...req.body,
            },
            function (error, coupon) {
                if (error) {
                    res.status(500).send({ status: 'error', message: error });
                } else {
                    res.send(coupon);
                }
            }
        );
    } catch (error) {
        res.status(500).send({ status: 'error', message: error });
    }
};

const createOneCoupon = async (request, response) => {
    try {
        // const { role, id, accessToken } = request.user
        const {
            codigo,
            tipo,
            caracter,
            estado,
            precioDescuento,
        } = request.body;

        await couponModel.create(
            {
                codigo,
                tipo,
                caracter,
                estado,
                precioDescuento,
            },
            function (error, coupon) {
                if (error) {
                    console.log(error);
                    if (error.code && error.code === 11000) {
                        response.status(400).send({
                            status: 'VALOR DUPLICADO',
                            message: error.keyValue,
                        });
                        return;
                    }
                    response
                        .status(500)
                        .send({ status: 'error', message: error.message });
                    return;
                } else {
                    response.send({
                        status: 'product was created',
                        data: coupon,
                    });
                    return;
                }
            }
        );
    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }
};

const createManyCoupon = async (request, response) => {
    try {
        const list_vouchers = request.body;
        // const { role } = request.user
        // var id = request.params.Id
        // var objectId = new ObjectId(request.params.Id)

        cuponModel.insertMany(
            list_vouchers,
            function (error, responseVouchers) {
                if (error) {
                    response
                        .status(500)
                        .send({ status: 'error', message: error.message });
                } else {
                    response.send({
                        status: 'product was created',
                        message: responseVouchers,
                    });
                    return;
                }
            }
        );
    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response.status(400).send({
                status: 'VALOR DUPLICADO-UNA DE LAS EMPRESAS NO HA SIDO CREADA',
                message: error.keyValue,
            });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }
};

const deleteOneCoupon = (req, res) => {
    cuponModel
        .findByIdAndDelete(req.params.cuponId)
        .then((name) => {
            if (!name) {
                return res.status(404).send({
                    message: 'product not found with id ' + req.params.cuponId,
                });
            }
            res.send({
                message:
                    'product ' + req.params.cuponId + ' deleted successfully!',
            });
        })
        .catch((err) => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: 'product not found with id ' + req.params.cuponId,
                });
            }
            return res.status(500).send({
                message:
                    'Could not delete product with id ' + req.params.cuponId,
            });
        });
};

module.exports = {
    getOneCoupon,
    getAllCoupon,
    updateOneCoupon,
    createOneCoupon,
    createManyCoupon,
    deleteOneCoupon,
};
