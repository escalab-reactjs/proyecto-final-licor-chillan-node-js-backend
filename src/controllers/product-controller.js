const jwt = require('jsonwebtoken');
const auth = require('../middlewares/auth');
const productModel = require('../mongo/models/product');




// -------- funciones



const getOneProduct = async (request, response) => {
    try {
        const { productId } = request.params;

        const product = await productModel.find({ _id: productId });


        response.status(200).send({ status: 'ok', data: product });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};

const getAllProducts = async (request, response) => {
    try {
        const products = await productModel.find();
        response.status(200).send({ status: 'ok', data: products });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error.message });
    }
};


const updateOneProduct = (request, response) => {
    // Validate Request

    try {

        if (!request.params.productId) {
            return response.status(400).send({
                message: "productId can not be empty"
            });
        }
        // Find empresa and update it with the request body
        console.log("request.params.productId" + request.params.productId);

        const { stock, name } = request.body

        productModel.findByIdAndUpdate(request.params.productId, {
            ...request.body

        }, function (error, product) {
            if (error) {

                response.status(500).send({ status: 'error', message: error });
            } else {


                response.send(product);


            }
        });
    } catch (error) {

        response.status(500).send({ status: 'error', message: error });
    }
};


const createOneProduct = async (request, response) => {
    try {

        // const { role, id, accessToken } = request.user
        console.log("data: " + JSON.stringify(request.body));
        const { name, stock, url_img, price_unitary, brand } = request.body;

        await productModel.create({
            name,
            stock,
            url_img,
            price_unitary,
            brand,
        }, function (error, product) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'product was created', data: product });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createOneProductWithImg = async (request, response) => {
    try {

        // const { role, id, accessToken } = request.user
        const { name, stock, price_unitary, brand } = request.body;
        const { file, body } = request

        console.log("file.path", file.path);
        console.log("body", body);
        const url_img = file.path;
        await productModel.create({
            name,
            stock,
            url_img,
            price_unitary,
            brand,
        }, function (error, product) {
            if (error) {
                console.log(error);
                if (error.code && error.code === 11000) {
                    response
                        .status(400)
                        .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
                    return;
                }
                response.status(500).send({ status: 'error', message: error.message });
                return;
            } else {
                response.send({ status: 'product was created', data: product });
                return;
            }

        });

    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }



}

const createManyProduct = async (request, response) => {
    try {
        const list_products = request.body;
        // const { role } = request.user
        // var id = request.params.Id
        // var objectId = new ObjectId(request.params.Id)


        productModel.insertMany(list_products, function (error, responseProducts) {
            if (error) {

                response.status(500).send({ status: 'error', message: error.message });
            } else {

                response.send({ status: 'product was created', message: responseProducts });
                return
            }
        })


    } catch (error) {
        console.log(error);
        if (error.code && error.code === 11000) {
            response
                .status(400)
                .send({ status: 'VALOR DUPLICADO-UNA DE LAS EMPRESAS NO HA SIDO CREADA', message: error.keyValue });
            return;
        }
        response.status(500).send({ status: 'error', message: error.message });
    }


}

const deleteOneOnlyProduct = (req, res) => {

    productModel.findByIdAndDelete(req.params.productId)
        .then(name => {
            if (!name) {
                return res.status(404).send({
                    message: "product not found with id " + req.params.productId
                });
            }
            res.send({ message: "product " + req.params.productId + " deleted successfully!" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === 'NotFound') {
                return res.status(404).send({
                    message: "product not found with id " + req.params.productId
                });
            }
            return res.status(500).send({
                message: "Could not delete product with id " + req.params.productId
            });
        });
};


module.exports = {
    getOneProduct,
    getAllProducts,
    updateOneProduct,
    createOneProduct,
    createOneProductWithImg,
    createManyProduct,
    deleteOneOnlyProduct,

};